class User {
    constructor(name, photo) {
        this.name = name
        this.photo = photo
    }

    getHiddenEmail() {
        return "as.....das@dasf.com"
    }
}

export { User }