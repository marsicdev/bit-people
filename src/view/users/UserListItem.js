import React from 'react'
import PropTypes from 'prop-types'

const UserListItem = ({ photo, name }) => {
    return (
        <li className="collection-item avatar">
            <img src={photo} alt="" className="circle" />
            <span className="title">{name}</span>
            <p>First Line</p>
            <p>First Line</p>
        </li>
    )
}

UserListItem.propTypes = {
    name: PropTypes.string.isRequired,
    photo: PropTypes.string
};

UserListItem.defaultProps = {
    photo: "https://images.pexels.com/photos/236047/pexels-photo-236047.jpeg?cs=srgb&dl=landscape-nature-sky-236047.jpg&fm=jpg"
};

export { UserListItem }
