import React, { Fragment } from 'react'
import { UserListItem } from './UserListItem';
import { UserCard } from './UserCard';

const UserList = (props) => {

    const { users, isGrid } = props;

    const userListItems = users.map((user, index) => (
        <UserListItem key={index} name={user.name} photo={user.photo} />
    ));

    const userCardItems = users.map((user) => {
        return <UserCard user={user} />
    })

    return (
        <Fragment >
            <ul className="collection">
                {isGrid ? userCardItems : userListItems}
            </ul>
        </Fragment>
    )
}

export { UserList }