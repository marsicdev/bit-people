import React from 'react'

const UserCard = ({ user }) => {
    return (
        <div className="card">
            <div className="card-image">
                <img src={user.photo} alt="" />
                <span className="card-title">{user.name}</span>
            </div>
        </div>
    )
}

export { UserCard }