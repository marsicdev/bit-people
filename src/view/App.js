import React, { Fragment, Component } from 'react';

import './App.css';

import { Header } from './common/Header';
import { UserList } from './users/UserList'
import { Footer } from './common/Footer';

import * as userService from './../service/userService'

class App extends Component {

  constructor(props) {
    super(props)
    console.log("constructor()");

    this.state = {
      isGrid: false,
      myUsers: []
    }
  }

  componentDidMount() {
    console.log("componentDidMount()");

    userService.fetchUserData()
      .then((myUsers) => {

        console.log("State changed");
        this.setState({ myUsers: myUsers })
      })

  }

  // Nasa metoda
  onChangeModeClick = (event) => {
    const isGrid = !this.state.isGrid

    console.log("State changed");
    this.setState({
      isGrid
    })
  }
  kz
  render() {
    console.log("render()");

    return (
      <Fragment>
        <Header title="BIT People" />
        <input className="btn" type="button" value="Change view mode" onClick={this.onChangeModeClick} />
        <main className="container">
          <UserList users={this.state.myUsers} isGrid={this.state.isGrid} />
        </main>
        <Footer />
      </Fragment>
    );
  }
}

export default App;
