import React from 'react'

const Header = (props) => {
    return (
        <header>
            <nav>
                <div className="nav-wrapper">
                    <a className="brand-logo center">{props.title}</a>
                </div>
            </nav>
        </header>
    )
}

export { Header }