import { peopleData } from './../data/mydata';
import { User } from "./../entities/User"
import { BASE_URL } from './../shared/constants'

const getLocalUserDataFromFile = () => {
    // get data from somewhere
    const peopleArray = peopleData.results
    
    // map data to entities
    const myUsers = peopleArray.map((user) => {
        return new User(user.name.first, user.picture.large)
    })

    // return data for usage
    return myUsers
}

const fetchUserData = () => {
    console.log("\tuserService: fetchUserData()");

    return fetch(BASE_URL)
        .then((response) => {
            return response.json()
        })
        .then((result) => {
            console.log("\tresult", result);
            const rawDataArray = result.results
            return rawDataArray
        })
        .then((rawUsers) => {
            const myUsers = rawUsers.map((user) => {
                return new User(user.name.first, user.picture.large)
            })

            console.log("\tmyUsers", myUsers)

            return myUsers
        })
}

export { getLocalUserDataFromFile, fetchUserData }
